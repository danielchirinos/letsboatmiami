<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = "Let's boat Miami | Bienvenido";
    $this->params['activeLink'] = "index";
?>

    <!-- Banner start -->
    <section class="banner">
        <div class=" owl-carousel hero-slider bg-shape">
            <div class="item" style="background-image: url('<?= Yii::getAlias('@web') ?>/images/slider/slider1.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-7">
                            <div class="banner-content">
                                <h2><span>Titulo</span> texto</h2>
                                <p>texto 2</p>
                                <a href="#" class="btn btn-default">leer mas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('<?= Yii::getAlias('@web') ?>/images/slider/slider2.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-7">
                            <div class="banner-content">
                                <h2><span>Titulo</span> texto</h2>
                                <p>texto 2</p>
                                <a href="#" class="btn btn-default">leer mas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background-image: url('<?= Yii::getAlias('@web') ?>/images/slider/s3.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-7">
                            <div class="banner-content">
                                <h2><span>Titulo</span> texto</h2>
                                <p>texto 2</p>
                                <a href="#" class="btn btn-default">leer mas</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!-- Banner start -->

<!--  -->

    <!-- Courses section start -->
    <section class="courses home bg-offwhite">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">Bienvenidos a</span>
                        <h3 class="sec-title">Let's boat Miami</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="entry-content">
                        <h3>titulo de bienvenida.</h3>
                        <p>texto de bienvenida.</p>

                        <h6>otra seccion.</h6>
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deleniti nesciunt expedita atque quibusdam ullam voluptatum non est quos aspernatur totam quisquam animi, voluptate odio et ex voluptatibus eaque, voluptas odit.</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="images/800x600.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Courses section end -->
<!--  -->

    <!-- Main form start -->
    <section class="main-form d-none">
        <div class="container">

            
            <div class="m-search-form nm-top-130">
                <form class="col-d-12">
                    <div class="row mt-0">
                        <div class="col-md-9 p-0">
                            <div class="row mt-0">
                                <span class="checkbox">
                                    <button class="btn bstn">Rent <i class="ti-check"></i></button>
                                </span>
                                <div class="input-group-append">
                                    <span class="input-group-text p-0">
                                        <select id="yachtname" data-style="custom-select bg-transparent border-0" data-container="body" data-live-search="true" class="selectpicker form-control bg-transparent">
                                            <optgroup label="Yacht name">
                                                <option value="">Yacht name</option>
                                                <option value="AE CAP D'ANTIBES" data-alias="ae-cap-dantibes">AE CAP D'ANTIBES</option>
                                                <option value="ALASKA OF GEORGE TOWN" data-alias="alaska-of-george-town">ALASKA OF GEORGE TOWN</option>
                                                <option value="ALESSANDRA" data-alias="alessandra">ALESSANDRA</option>
                                                <option value="ALL ABOUT U2" data-alias="all-about-u2">ALL ABOUT U2</option>
                                                <option value="ALUCIA" data-alias="alucia">ALUCIA</option>
                                                <option value="AMADEUS" data-alias="amadeus">AMADEUS</option>
                                                <option value="ANAMCARA" data-alias="anamcara">ANAMCARA</option>
                                                <option value="ANDREA CAY" data-alias="andrea-cay">ANDREA CAY</option>
                                                <option value="ANTARA" data-alias="antara">ANTARA</option>
                                                <option value="APHRODITE A" data-alias="aphrodite-a">APHRODITE A</option>
                                                <option value="ARTPOLARS" data-alias="artpolars">ARTPOLARS</option>
                                                <option value="ASIA" data-alias="asia">ASIA</option>
                                                <option value="ASMAA" data-alias="asmaa">ASMAA</option>
                                                <option value="ATLANTIC" data-alias="atlantic">ATLANTIC</option>
                                                <option value="AURORA BOREALIS" data-alias="aurora-borealis">AURORA BOREALIS AMELS 220</option>
                                                <option value="AVICCI" data-alias="avicci">AVICCI</option>
                                                <option value="AZIMUT 77S" data-alias="azimut-77s">AZIMUT 77S</option>
                                                <option value="B.NOW" data-alias="b-now">B.NOW</option>
                                                <option value="BARBARA" data-alias="barbara">BARBARA</option>
                                                <option value="BE COOL 2" data-alias="be-cool-2">BE COOL 2</option>
                                                <option value="BEAUPORT" data-alias="beauport">BEAUPORT</option>
                                                <option value="BENETTI 50M FB 215" data-alias="benetti-50m-fb-215">BENETTI 50M FB 215</option>
                                                <option value="BLACKCAT 35" data-alias="blackcat-35">BLACKCAT 35</option>
                                                <option value="BLACKCAT 50" data-alias="blackcat-50">BLACKCAT 50</option>
                                                <option value="BLUE" data-alias="blue">BLUE</option>
                                                <option value="BLUE TOO" data-alias="blue-too">BLUE TOO</option>
                                                <option value="BOADICEA" data-alias="boadicea">BOADICEA</option>
                                                <option value="BUCEPHALUS" data-alias="bucephalus">BUCEPHALUS</option>
                                                <option value="CARAMIA" data-alias="caramia">CARAMIA</option>
                                                <option value="CHALLENGE" data-alias="challenge">CHALLENGE</option>
                                                <option value="CLUB M.SEA" data-alias="club-m-sea">CLUB M.SEA</option>
                                                <option value="COLUMBO BREEZE" data-alias="columbo-breeze">COLUMBO BREEZE</option>
                                                <option value="CORROBOREE" data-alias="corroboree">CORROBOREE</option>
                                                <option value="CRACKER BAY" data-alias="cracker-bay">CRACKER BAY</option>
                                                <option value="DANNESKJOLD" data-alias="danneskjold">DANNESKJOLD</option>
                                                <option value="DANUSH" data-alias="danush">DANUSH</option>
                                                <option value="DAY OFF" data-alias="day-off">DAY OFF</option>
                                                <option value="DEEP STORY" data-alias="deep-story">DEEP STORY</option>
                                                <option value="DOLCE VITA" data-alias="dolce-vita">DOLCE VITA</option>
                                                <option value="DR NO NO" data-alias="dr-no-no-1">DR NO NO</option>
                                                <option value="DRAGON" data-alias="dragon-2">DRAGON</option>
                                                <option value="DREAMER" data-alias="dreamer">DREAMER</option>
                                                <option value="DREAMLAND" data-alias="dreamland">DREAMLAND</option>
                                                <option value="ELBA" data-alias="elba">ELBA</option>
                                                <option value="ELEMENTS" data-alias="elements">ELEMENTS</option>
                                                <option value="ENCORE" data-alias="encore-1">ENCORE</option>
                                                <option value="ENDEAVOUR II" data-alias="endeavour-ii-1">ENDEAVOUR II</option>
                                                <option value="ETOUPE" data-alias="etoupe">ETOUPE</option>
                                                <option value="EUPHORIA" data-alias="mayra-50m">EUPHORIA</option>
                                                <option value="FLEURTJE" data-alias="fleurtje">FLEURTJE</option>
                                                <option value="GAME HOG DOS" data-alias="game-hog-dos">GAME HOG DOS</option>
                                                <option value="GEORGIA" data-alias="georgia">GEORGIA</option>
                                                <option value="GIANNELLA" data-alias="giannella">GIANNELLA</option>
                                                <option value="GOOD CALL" data-alias="good-call">GOOD CALL</option>
                                                <option value="GRAND RUSALINA" data-alias="grand-rusalina">GRAND RUSALINA</option>
                                                <option value="HAPPY HOUR" data-alias="happy-hour-1">HAPPY HOUR</option>
                                                <option value="HARMONY III" data-alias="harmony-iii">HARMONY III</option>
                                                <option value="HERCULES I" data-alias="hercules-i">HERCULES I</option>
                                                <option value="HERE COMES THE SUN" data-alias="here-comes-the-sun">HERE COMES THE SUN</option>
                                                <option value="IRELANDA" data-alias="irelanda">IRELANDA</option>
                                                <option value="ITASCA" data-alias="itasca">ITASCA</option>
                                                <option value="KARIMA" data-alias="karima">KARIMA</option>
                                                <option value="KATERINA" data-alias="katerina">KATERINA</option>
                                                <option value="Ke Ama II" data-alias="ke-ama-ii">Ke Ama II</option>
                                                <option value="L'ALBATROS" data-alias="l-albatros-1">L'ALBATROS</option>
                                                <option value="LADY ELLA S" data-alias="lady-ella-s">LADY ELLA S SUNSEEKER 116</option>
                                                <option value="LE PIETRE" data-alias="le-pietre">LE PIETRE</option>
                                                <option value="LUC AN" data-alias="luc-an">LUC AN</option>
                                                <option value="LUNAR" data-alias="lunar">LUNAR</option>
                                                <option value="M.T. TIME" data-alias="m-t-time">M.T. TIME</option>
                                                <option value="MAD MAX X" data-alias="mad-max-x">MAD MAX X</option>
                                                <option value="MAG III" data-alias="mag-iii">MAG III</option>
                                                <option value="MAGNUM" data-alias="magnum">MAGNUM</option>
                                                <option value="MAI" data-alias="mai">MAI</option>
                                                <option value="MALVASIA" data-alias="malvasia">MALVASIA</option>
                                                <option value="MARILEE" data-alias="marilee">MARILEE</option>
                                                <option value="MAYAMA" data-alias="mayama">MAYAMA</option>
                                                <option value="MERMAID" data-alias="mermaid">MERMAID</option>
                                                <option value="MINX" data-alias="minx">MINX</option>
                                                <option value="MIRABELLA" data-alias="mirabella">MIRABELLA</option>
                                                <option value="MONTREVEL" data-alias="montrevel">MONTREVEL</option>
                                                <option value="MS MIGRATION" data-alias="ms-migration">MS MIGRATION</option>
                                                <option value="MY TUTKU" data-alias="my-tutku">MY TUTKU</option>
                                                <option value="NADAN" data-alias="nadan">NADAN</option>
                                                <option value="NASSIMA" data-alias="nassima">NASSIMA</option>
                                                <option value="NAUTA" data-alias="nauta">NAUTA</option>
                                                <option value="NIRVANA" data-alias="nirvana-gulf-craft">NIRVANA</option>
                                                <option value="NITTANY LION" data-alias="nittany-lion">NITTANY LION</option>
                                                <option value="NO LIMIT" data-alias="no-limit">NO LIMIT</option>
                                                <option value="OCTOPUS" data-alias="octopus">OCTOPUS</option>
                                                <option value="ODIN II" data-alias="odin-ii-1">ODIN II</option>
                                                <option value="ODYSSEUS" data-alias="odysseus">ODYSSEUS</option>
                                                <option value="ODYSSEY" data-alias="odyssey">ODYSSEY</option>
                                                <option value="OGUZ KHAN" data-alias="oguz-khan">OGUZ KHAN</option>
                                                <option value="OHANA" data-alias="ohana">OHANA</option>
                                                <option value="OLMIDA" data-alias="olmida">OLMIDA</option>
                                                <option value="OYA" data-alias="oya">OYA</option>
                                                <option value="PACIFIC PROVIDER" data-alias="pacific-provider">PACIFIC PROVIDER</option>
                                                <option value="PATH" data-alias="path">PATH</option>
                                                <option value="PERSEUS 3" data-alias="perseus-3">PERSEUS^3</option>
                                                <option value="PLANET NINE" data-alias="planet-nine">PLANET NINE</option>
                                                <option value="PRINCESS 78" data-alias="princess-78">PRINCESS 78</option>
                                                <option value="PROJECT MARLIN" data-alias="project-marlin">PROJECT MARLIN</option>
                                                <option value="QUASAR" data-alias="quasar">QUASAR</option>
                                                <option value="QUEEN ANNE" data-alias="queen-anne">QUEEN ANNE</option>
                                                <option value="QUITE ESSENTIAL" data-alias="quite-essential">QUITE ESSENTIAL</option>
                                                <option value="REGULUS" data-alias="regulus">REGULUS</option>
                                                <option value="SCOUT II" data-alias="scout-ii">SCOUT II</option>
                                                <option value="SEA HAWK" data-alias="sea-hawk">SEA HAWK</option>
                                                <option value="SEAS 64" data-alias="seas-64">SEAS 64</option>
                                                <option value="SEYCHELLE" data-alias="seychelle">SEYCHELLE</option>
                                                <option value="SHENANDOAH OF SARK" data-alias="shenandoah-of-sark">SHENANDOAH OF SARK</option>
                                                <option value="SHU SHE" data-alias="shu-she">SHU SHE</option>
                                                <option value="SILENTWORLD" data-alias="silentworld">SILENTWORLD</option>
                                                <option value="SIMA" data-alias="sima">SIMA</option>
                                                <option value="SLOW DANCE" data-alias="slow-dance">SLOW DANCE</option>
                                                <option value="SNOWBORED" data-alias="snowbored">SNOWBORED</option>
                                                <option value="SOCORRO" data-alias="socorro">SOCORRO</option>
                                                <option value="SOLAIA" data-alias="solaia">SOLAIA</option>
                                                <option value="SPOOM" data-alias="spoom">SPOOM</option>
                                                <option value="SPRING" data-alias="spring">SPRING</option>
                                                <option value="STATE OF GRACE" data-alias="state-of-grace">STATE OF GRACE</option>
                                                <option value="SYCARA V" data-alias="sycara-v">SYCARA V</option>
                                                <option value="SYL" data-alias="syl">SYL</option>
                                                <option value="TAKE 5" data-alias="take-5-monte-fino">TAKE 5</option>
                                                <option value="TAWERA" data-alias="tawera">TAWERA</option>
                                                <option value="TENACIOUS" data-alias="tenacious">TENACIOUS</option>
                                                <option value="TOBY" data-alias="toby">TOBY</option>
                                                <option value="TOTO" data-alias="toto">TOTO</option>
                                                <option value="VAYU" data-alias="vayu">VAYU</option>
                                                <option value="VICIOUS RUMOUR II" data-alias="vicious-rumour-ii-1">VICIOUS RUMOUR II</option>
                                                <option value="VIRTUS 44" data-alias="virtus-44">VIRTUS 44</option>
                                                <option value="VODKA" data-alias="vodka">VODKA</option>
                                                <option value="WHEELS 1" data-alias="wheels-1">WHEELS (HORIZON)</option>
                                                <option value="WILD CHILD" data-alias="wild-child">WILD CHILD</option>
                                                <option value="ZAMBOANGA" data-alias="zamboanga">ZAMBOANGA</option>
                                                <option value="ZWEISAMKEIT" data-alias="zweisamkeit">ZWEISAMKEIT</option>
                                            </optgroup>
                                        </select>
                                    </span>
                                </div>
                                <div class="input-group-append mr-1">
                                    <span class="input-group-text p-0">
                                        <select id="location" data-style="custom-select bg-transparent border-0" data-container="body" data-live-search="true" class="selectpicker form-control bg-transparent">
                                            <optgroup label="Select Location">
                                                <option value="">Select Country</option>
                                                <option value="244">Aaland Islands</option>
                                                <option value="1">Afghanistan</option>
                                                <option value="2">Albania</option>
                                                <option value="3">Algeria</option>
                                                <option value="4">American Samoa</option>
                                                <option value="5">Andorra</option>
                                                <option value="6">Angola</option>
                                                <option value="7">Anguilla</option>
                                                <option value="8">Antarctica</option>
                                                <option value="9">Antigua and Barbuda</option>
                                                <option value="10">Argentina</option>
                                                <option value="11">Armenia</option>
                                                <option value="12">Aruba</option>
                                                <option value="252">Ascension Island (British)</option>
                                                <option value="13">Australia</option>
                                                <option value="14">Austria</option>
                                                <option value="15">Azerbaijan</option>
                                                <option value="16">Bahamas</option>
                                                <option value="17">Bahrain</option>
                                                <option value="18">Bangladesh</option>
                                                <option value="19">Barbados</option>
                                                <option value="20">Belarus</option>
                                                <option value="21">Belgium</option>
                                                <option value="22">Belize</option>
                                                <option value="23">Benin</option>
                                                <option value="24">Bermuda</option>
                                                <option value="25">Bhutan</option>
                                                <option value="26">Bolivia</option>
                                                <option value="245">Bonaire, Sint Eustatius and Saba</option>
                                                <option value="27">Bosnia and Herzegovina</option>
                                                <option value="28">Botswana</option>
                                                <option value="29">Bouvet Island</option>
                                                <option value="30">Brazil</option>
                                                <option value="31">British Indian Ocean Territory</option>
                                                <option value="32">Brunei Darussalam</option>
                                                <option value="33">Bulgaria</option>
                                                <option value="34">Burkina Faso</option>
                                                <option value="35">Burundi</option>
                                                <option value="36">Cambodia</option>
                                                <option value="37">Cameroon</option>
                                                <option value="38">Canada</option>
                                                <option value="251">Canary Islands</option>
                                                <option value="39">Cape Verde</option>
                                                <option value="40">Cayman Islands</option>
                                                <option value="41">Central African Republic</option>
                                                <option value="42">Chad</option>
                                                <option value="43">Chile</option>
                                                <option value="44">China</option>
                                                <option value="45">Christmas Island</option>
                                                <option value="46">Cocos (Keeling) Islands</option>
                                                <option value="47">Colombia</option>
                                                <option value="48">Comoros</option>
                                                <option value="49">Congo</option>
                                                <option value="50">Cook Islands</option>
                                                <option value="51">Costa Rica</option>
                                                <option value="52">Cote D'Ivoire</option>
                                                <option value="53">Croatia</option>
                                                <option value="54">Cuba</option>
                                                <option value="246">Curacao</option>
                                                <option value="55">Cyprus</option>
                                                <option value="56">Czech Republic</option>
                                                <option value="237">Democratic Republic of Congo</option>
                                                <option value="57">Denmark</option>
                                                <option value="58">Djibouti</option>
                                                <option value="59">Dominica</option>
                                                <option value="60">Dominican Republic</option>
                                                <option value="61">East Timor</option>
                                                <option value="62">Ecuador</option>
                                                <option value="63">Egypt</option>
                                                <option value="64">El Salvador</option>
                                                <option value="65">Equatorial Guinea</option>
                                                <option value="66">Eritrea</option>
                                                <option value="67">Estonia</option>
                                                <option value="68">Ethiopia</option>
                                                <option value="69">Falkland Islands (Malvinas)</option>
                                                <option value="70">Faroe Islands</option>
                                                <option value="71">Fiji</option>
                                                <option value="72">Finland</option>
                                                <option value="74">France, Metropolitan</option>
                                                <option value="75">French Guiana</option>
                                                <option value="76">French Polynesia</option>
                                                <option value="77">French Southern Territories</option>
                                                <option value="126">FYROM</option>
                                                <option value="78">Gabon</option>
                                                <option value="79">Gambia</option>
                                                <option value="80">Georgia</option>
                                                <option value="81">Germany</option>
                                                <option value="82">Ghana</option>
                                                <option value="83">Gibraltar</option>
                                                <option value="84">Greece</option>
                                                <option value="85">Greenland</option>
                                                <option value="86">Grenada</option>
                                                <option value="87">Guadeloupe</option>
                                                <option value="88">Guam</option>
                                                <option value="89">Guatemala</option>
                                                <option value="256">Guernsey</option>
                                                <option value="90">Guinea</option>
                                                <option value="91">Guinea-Bissau</option>
                                                <option value="92">Guyana</option>
                                                <option value="93">Haiti</option>
                                                <option value="94">Heard and Mc Donald Islands</option>
                                                <option value="95">Honduras</option>
                                                <option value="96">Hong Kong</option>
                                                <option value="97">Hungary</option>
                                                <option value="98">Iceland</option>
                                                <option value="99">India</option>
                                                <option value="100">Indonesia</option>
                                                <option value="101">Iran (Islamic Republic of)</option>
                                                <option value="102">Iraq</option>
                                                <option value="103">Ireland</option>
                                                <option value="254">Isle of Man</option>
                                                <option value="104">Israel</option>
                                                <option value="105">Italy</option>
                                                <option value="106">Jamaica</option>
                                                <option value="107">Japan</option>
                                                <option value="257">Jersey</option>
                                                <option value="108">Jordan</option>
                                                <option value="109">Kazakhstan</option>
                                                <option value="110">Kenya</option>
                                                <option value="111">Kiribati</option>
                                                <option value="113">Korea, Republic of</option>
                                                <option value="253">Kosovo, Republic of</option>
                                                <option value="114">Kuwait</option>
                                                <option value="115">Kyrgyzstan</option>
                                                <option value="116">Lao People's Democratic Republic</option>
                                                <option value="117">Latvia</option>
                                                <option value="118">Lebanon</option>
                                                <option value="119">Lesotho</option>
                                                <option value="120">Liberia</option>
                                                <option value="121">Libyan Arab Jamahiriya</option>
                                                <option value="122">Liechtenstein</option>
                                                <option value="123">Lithuania</option>
                                                <option value="124">Luxembourg</option>
                                                <option value="125">Macau</option>
                                                <option value="127">Madagascar</option>
                                                <option value="128">Malawi</option>
                                                <option value="129">Malaysia</option>
                                                <option value="130">Maldives</option>
                                                <option value="131">Mali</option>
                                                <option value="132">Malta</option>
                                                <option value="133">Marshall Islands</option>
                                                <option value="134">Martinique</option>
                                                <option value="135">Mauritania</option>
                                                <option value="136">Mauritius</option>
                                                <option value="137">Mayotte</option>
                                                <option value="138">Mexico</option>
                                                <option value="139">Micronesia, Federated States of</option>
                                                <option value="140">Moldova, Republic of</option>
                                                <option value="141">Monaco</option>
                                                <option value="142">Mongolia</option>
                                                <option value="242">Montenegro</option>
                                                <option value="143">Montserrat</option>
                                                <option value="144">Morocco</option>
                                                <option value="145">Mozambique</option>
                                                <option value="146">Myanmar</option>
                                                <option value="147">Namibia</option>
                                                <option value="148">Nauru</option>
                                                <option value="149">Nepal</option>
                                                <option value="150">Netherlands</option>
                                                <option value="151">Netherlands Antilles</option>
                                                <option value="152">New Caledonia</option>
                                                <option value="153">New Zealand</option>
                                                <option value="154">Nicaragua</option>
                                                <option value="155">Niger</option>
                                                <option value="156">Nigeria</option>
                                                <option value="157">Niue</option>
                                                <option value="158">Norfolk Island</option>
                                                <option value="112">North Korea</option>
                                                <option value="159">Northern Mariana Islands</option>
                                                <option value="160">Norway</option>
                                                <option value="161">Oman</option>
                                                <option value="162">Pakistan</option>
                                                <option value="163">Palau</option>
                                                <option value="247">Palestinian Territory, Occupied</option>
                                                <option value="164">Panama</option>
                                                <option value="165">Papua New Guinea</option>
                                                <option value="166">Paraguay</option>
                                                <option value="167">Peru</option>
                                                <option value="168">Philippines</option>
                                                <option value="169">Pitcairn</option>
                                                <option value="170">Poland</option>
                                                <option value="171">Portugal</option>
                                                <option value="172">Puerto Rico</option>
                                                <option value="173">Qatar</option>
                                                <option value="174">Reunion</option>
                                                <option value="175">Romania</option>
                                                <option value="176">Russian Federation</option>
                                                <option value="177">Rwanda</option>
                                                <option value="178">Saint Kitts and Nevis</option>
                                                <option value="179">Saint Lucia</option>
                                                <option value="180">Saint Vincent and the Grenadines</option>
                                                <option value="181">Samoa</option>
                                                <option value="182">San Marino</option>
                                                <option value="183">Sao Tome and Principe</option>
                                                <option value="184">Saudi Arabia</option>
                                                <option value="185">Senegal</option>
                                                <option value="243">Serbia</option>
                                                <option value="186">Seychelles</option>
                                                <option value="187">Sierra Leone</option>
                                                <option value="188">Singapore</option>
                                                <option value="189">Slovak Republic</option>
                                                <option value="190">Slovenia</option>
                                                <option value="191">Solomon Islands</option>
                                                <option value="192">Somalia</option>
                                                <option value="193">South Africa</option>
                                                <option value="194">South Georgia &amp; South Sandwich Islands
                                                </option>
                                                <option value="248">South Sudan</option>
                                                <option value="195">Spain</option>
                                                <option value="196">Sri Lanka</option>
                                                <option value="249">St. Barthelemy</option>
                                                <option value="197">St. Helena</option>
                                                <option value="250">St. Martin (French part)</option>
                                                <option value="198">St. Pierre and Miquelon</option>
                                                <option value="199">Sudan</option>
                                                <option value="200">Suriname</option>
                                                <option value="201">Svalbard and Jan Mayen Islands</option>
                                                <option value="202">Swaziland</option>
                                                <option value="203">Sweden</option>
                                                <option value="204">Switzerland</option>
                                                <option value="205">Syrian Arab Republic</option>
                                                <option value="206">Taiwan</option>
                                                <option value="207">Tajikistan</option>
                                                <option value="208">Tanzania, United Republic of</option>
                                                <option value="209">Thailand</option>
                                                <option value="210">Togo</option>
                                                <option value="211">Tokelau</option>
                                                <option value="212">Tonga</option>
                                                <option value="213">Trinidad and Tobago</option>
                                                <option value="255">Tristan da Cunha</option>
                                                <option value="214">Tunisia</option>
                                                <option value="215">Turkey</option>
                                                <option value="216">Turkmenistan</option>
                                                <option value="217">Turks and Caicos Islands</option>
                                                <option value="218">Tuvalu</option>
                                                <option value="219">Uganda</option>
                                                <option value="220">Ukraine</option>
                                                <option value="221">United Arab Emirates</option>
                                                <option value="222">United Kingdom</option>
                                                <option value="223">United States</option>
                                                <option value="224">United States Minor Outlying Islands</option>
                                                <option value="225">Uruguay</option>
                                                <option value="226">Uzbekistan</option>
                                                <option value="227">Vanuatu</option>
                                                <option value="228">Vatican City State (Holy See)</option>
                                                <option value="229">Venezuela</option>
                                                <option value="230">Viet Nam</option>
                                                <option value="231">Virgin Islands (British)</option>
                                                <option value="232">Virgin Islands (U.S.)</option>
                                                <option value="233">Wallis and Futuna Islands</option>
                                                <option value="234">Western Sahara</option>
                                                <option value="235">Yemen</option>
                                                <option value="238">Zambia</option>
                                                <option value="239">Zimbabwe</option>
                                            </optgroup>
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 p-0">
                            <div class="button-area">
                                <button type="submit" class="btn px-3 search-btn"><i class="ti-search"></i></button>
                                <button class="btn px-3 a-search">Advance <i class="ti-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="advance-search-area">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="control-group">
                                        <span>TYPE OF YACHT:</span>

                                        <div class="boxes">
                                            <input type="checkbox" id="box-1">
                                            <label for="box-1">MOTOR</label>

                                            <input type="checkbox" id="box-2" checked>
                                            <label for="box-2">SAIL</label>
                                        </div>

                                    </div>
                                    <div class="control-group">
                                        <span>LENGTH M/FT</span>
                                        <section class="range-slider" id="length" data-options='{"output":{"prefix":"M "},"maxSymbol":"+"}'>

                                            <input name="range-1" value="0" min="0" max="150" step="1" type="range">
                                            <input name="range-2" value="150" min="0" max="150" step="1" type="range">
                                        </section>
                                    </div>
                                    <div class="control-group">
                                        <span>PRICE PER/WEEK</span>
                                        <section class="range-slider" id="price" data-options='{"output":{"prefix":"$"},"maxSymbol":"+"}'>

                                            <input name="range-1" value="0" min="0" max="1250" step="1" type="range">
                                            <input name="range-2" value="1250" min="0" max="1250" step="1" type="range">
                                        </section>
                                    </div>
                                </div>
                                <div class="row">
                                    <select name="guests" class="cselect">
                                        <option value="0">No. Of Guests (Any)</option>
                                        <option value="6">Up to 6</option>
                                        <option value="8">6 - 8</option>
                                        <option value="10">8 - 10</option>
                                        <option value="12">10 - 12</option>
                                        <option value="999">12+</option>
                                    </select>
                                    <select name="build" class="cselect">
                                        <option value="-1">Built (All)</option>
                                        <option value="0">New build</option>
                                        <option value="2">Up to 2 yrs</option>
                                        <option value="5">Up to 5 yrs</option>
                                        <option value="10">Up to 10 yrs</option>
                                        <option value="99">Over 10 yrs</option>
                                    </select>
                                    <select name="builder" class="select2 width-215 select2-hidden-accessible cselect" tabindex="-1" aria-hidden="true">
                                        <option value="">Builder (Any)</option>
                                        <option value="Abd Aluminum">Abd Aluminum</option>
                                        <option value="Abeking &amp; Rasmussen">Abeking &amp; Rasmussen</option>
                                        <option value="Acico Yachts">Acico Yachts</option>
                                        <option value="Ada Yacht Works">Ada Yacht Works</option>
                                        <option value="Admiral">Admiral</option>
                                        <option value="Alia Yachts">Alia Yachts</option>
                                        <option value="Alloy Yachts">Alloy Yachts</option>
                                        <option value="Amels">Amels</option>
                                        <option value="Arno">Arno</option>
                                        <option value="Astilleros Armon">Astilleros Armon</option>
                                        <option value="Astilleros M. Cies">Astilleros M. Cies</option>
                                        <option value="Auroux">Auroux</option>
                                        <option value="Austal">Austal</option>
                                        <option value="Azimut">Azimut</option>
                                        <option value="Baglietto">Baglietto</option>
                                        <option value="Baia">Baia</option>
                                        <option value="Balk Shipyard">Balk Shipyard</option>
                                        <option value="Baltic Yachts">Baltic Yachts</option>
                                        <option value="Barcos Deportivos">Barcos Deportivos</option>
                                        <option value="Benetti">Benetti</option>
                                        <option value="Blount Marine">Blount Marine</option>
                                        <option value="Bodrum Oguz Marin">Bodrum Oguz Marin</option>
                                        <option value="Brooke Yachts">Brooke Yachts</option>
                                        <option value="Burger">Burger</option>
                                        <option value="Camper &amp; Nicholsons">Camper &amp; Nicholsons</option>
                                        <option value="Cavusoglu Shipyard">Cavusoglu Shipyard</option>
                                        <option value="CBI Navi">CBI Navi</option>
                                        <option value="Cerri Marine">Cerri Marine</option>
                                        <option value="CH Marine">CH Marine</option>
                                        <option value="CIM">CIM</option>
                                        <option value="CN Termoli">CN Termoli</option>
                                        <option value="Codecasa">Codecasa</option>
                                        <option value="Concorde Yachts">Concorde Yachts</option>
                                        <option value="Conrad Shipyard">Conrad Shipyard</option>
                                        <option value="COUACH">COUACH</option>
                                        <option value="CRN Ancona">CRN Ancona</option>
                                        <option value="Culham Eng. Whangarei">Culham Eng. Whangarei</option>
                                        <option value="Custom Built">Custom Built</option>
                                        <option value="Cyrus Yachts">Cyrus Yachts</option>
                                        <option value="Davie &amp; Sons">Davie &amp; Sons</option>
                                        <option value="De Vries Lentsch">De Vries Lentsch</option>
                                        <option value="DeBirs Yachts">DeBirs Yachts</option>
                                        <option value="DeFever">DeFever</option>
                                        <option value="Delta Marine">Delta Marine</option>
                                        <option value="Don Brooke">Don Brooke</option>
                                        <option value="Dragos Yachts">Dragos Yachts</option>
                                        <option value="Dunya">Dunya</option>
                                        <option value="Dynamiq Yachts">Dynamiq Yachts</option>
                                        <option value="Fassmer">Fassmer</option>
                                        <option value="Feadship">Feadship</option>
                                        <option value="Ferretti">Ferretti</option>
                                        <option value="Ferretti Custom Line">Ferretti Custom Line</option>
                                        <option value="Fitzroy Yachts">Fitzroy Yachts</option>
                                        <option value="Forges de l'Ouest">Forges de l'Ouest</option>
                                        <option value="Gentech Yachts">Gentech Yachts</option>
                                        <option value="Gideon">Gideon</option>
                                        <option value="Gulf Craft Inc">Gulf Craft Inc</option>
                                        <option value="Hakvoort">Hakvoort</option>
                                        <option value="Halter Marine">Halter Marine</option>
                                        <option value="Hargrave">Hargrave</option>
                                        <option value="Hatteras">Hatteras</option>
                                        <option value="Heesen">Heesen</option>
                                        <option value="Herreshoff">Herreshoff</option>
                                        <option value="Horizon Yachts">Horizon Yachts</option>
                                        <option value="Icon Yachts">Icon Yachts</option>
                                        <option value="IHC/Icon">IHC/Icon</option>
                                        <option value="Intermarine">Intermarine</option>
                                        <option value="ISA">ISA</option>
                                        <option value="J Boats">J Boats</option>
                                        <option value="J Craft">J Craft</option>
                                        <option value="J&amp;K Smit">J&amp;K Smit</option>
                                        <option value="JFA Yachts">JFA Yachts</option>
                                        <option value="Jongert">Jongert</option>
                                        <option value="Kha Shing">Kha Shing</option>
                                        <option value="Konjo Boat Builders Ara">Konjo Boat Builders Ara</option>
                                        <option value="Leight Notika">Leight Notika</option>
                                        <option value="Liman">Liman</option>
                                        <option value="Lloyd's Ships">Lloyd's Ships</option>
                                        <option value="Lurssen">Lurssen</option>
                                        <option value="LYNX">LYNX</option>
                                        <option value="Mayra Yachts Ltd.">Mayra Yachts Ltd.</option>
                                        <option value="McMullen &amp; Wing">McMullen &amp; Wing</option>
                                        <option value="Mengi Yay">Mengi Yay</option>
                                        <option value="Mie Shipyard">Mie Shipyard</option>
                                        <option value="Mitsubishi">Mitsubishi</option>
                                        <option value="Mondomarine">Mondomarine</option>
                                        <option value="Monte Carlo Yachts">Monte Carlo Yachts</option>
                                        <option value="Monte Fino">Monte Fino</option>
                                        <option value="Moonen">Moonen</option>
                                        <option value="Nautor's Swan">Nautor's Swan</option>
                                        <option value="Neorion">Neorion</option>
                                        <option value="New Zealand">New Zealand</option>
                                        <option value="Nobiskrug">Nobiskrug</option>
                                        <option value="Northcoast Yachts">Northcoast Yachts</option>
                                        <option value="Northern Marine">Northern Marine</option>
                                        <option value="Numarine">Numarine</option>
                                        <option value="Ocean Pacifico">Ocean Pacifico</option>
                                        <option value="Ocean Voyager">Ocean Voyager</option>
                                        <option value="Oceanco">Oceanco</option>
                                        <option value="Oceanfast">Oceanfast</option>
                                        <option value="Orkun Yachts">Orkun Yachts</option>
                                        <option value="Overmarine">Overmarine</option>
                                        <option value="Oyster Marine">Oyster Marine</option>
                                        <option value="Pachoud Motor Yachts">Pachoud Motor Yachts</option>
                                        <option value="Palmer Johnson">Palmer Johnson</option>
                                        <option value="Palumbo">Palumbo</option>
                                        <option value="PALUMBO SUPER YACHTS">PALUMBO SUPER YACHTS</option>
                                        <option value="Peri Yachts">Peri Yachts</option>
                                        <option value="Perini Navi">Perini Navi</option>
                                        <option value="Phithak Shipyard">Phithak Shipyard</option>
                                        <option value="Picchiotti">Picchiotti</option>
                                        <option value="Princess">Princess</option>
                                        <option value="Queenship">Queenship</option>
                                        <option value="Renaissance Yachts">Renaissance Yachts</option>
                                        <option value="Riva">Riva</option>
                                        <option value="Rossinavi">Rossinavi</option>
                                        <option value="Royal Denship">Royal Denship</option>
                                        <option value="Royal Huisman">Royal Huisman</option>
                                        <option value="Sangermani">Sangermani</option>
                                        <option value="Sanlorenzo">Sanlorenzo</option>
                                        <option value="Selene">Selene</option>
                                        <option value="Sermons">Sermons</option>
                                        <option value="Shipworks Brisbane">Shipworks Brisbane</option>
                                        <option value="Siar Moschini">Siar Moschini</option>
                                        <option value="Southern Ocean">Southern Ocean</option>
                                        <option value="Sparkman &amp; Stephens">Sparkman &amp; Stephens</option>
                                        <option value="Sterling">Sterling</option>
                                        <option value="Su Marine">Su Marine</option>
                                        <option value="Sunseeker">Sunseeker</option>
                                        <option value="Townsend-Downey">Townsend-Downey</option>
                                        <option value="Trident">Trident</option>
                                        <option value="Trinity">Trinity</option>
                                        <option value="Turkish built">Turkish built</option>
                                        <option value="Turquoise">Turquoise</option>
                                        <option value="Van Dam Nordia">Van Dam Nordia</option>
                                        <option value="Van De Graaf B.V.">Van De Graaf B.V.</option>
                                        <option value="Viareggio Superyachts">Viareggio Superyachts</option>
                                        <option value="Viking">Viking</option>
                                        <option value="Viking Princess">Viking Princess</option>
                                        <option value="Vitters">Vitters</option>
                                        <option value="Vuyk en Zonen">Vuyk en Zonen</option>
                                        <option value="Warren Yachts">Warren Yachts</option>
                                        <option value="Westport">Westport</option>
                                        <option value="Yachtley">Yachtley</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- Main form end -->

    <!-- Feature section start -->
    <section class="feature pt-5">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">Nuestros</span>
                        <h3 class="sec-title">Servicios</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="iconBox text-center">
                        <i class="flaticon-cruise"></i>
                        <!-- <img src="images/icons/1.png" alt="" class="img-icon"> -->
                        <h5><a href="#">Servicio 1</a></h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab laboriosam, alias totam odio dolore esse repellendus fugit at iste voluptas earum, fugiat molestias repudiandae adipisci maxime, odit cupiditate sunt possimus?</p>
                        <a href="#" class="read-more">Leer mas</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="iconBox text-center">
                        <!-- <img src="images/icons/2.png" alt="" class="img-icon"> -->
                        <i class="flaticon-ship-2"></i>
                        <h5><a href="#">Servicio 2</a></h5>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Cupiditate delectus sint ducimus magnam eligendi labore minima! Et sit sint sequi eaque, cupiditate eos aut! Cumque earum amet necessitatibus libero minus.</p>
                        <a href="#" class="read-more">Leer mas</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="iconBox text-center">
                        <!-- <img src="images/icons/3.png" alt="" class="img-icon"> -->
                        <i class="flaticon-yachting"></i>
                        <h5><a href="#">Servicio 3</a></h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloribus unde sed autem, ratione soluta nostrum dignissimos in blanditiis inventore necessitatibus cupiditate pariatur rem, ex aliquam vel reiciendis, ipsum molestias voluptatum.</p>
                        <a href="#" class="read-more">Leer mas</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Feature section end -->

    <!-- Courses section start -->
    <section class="courses home bg-offwhite d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">Yacht</span>
                        <h3 class="sec-title">Buy or Rent Choose Your favourite Boat</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-course">
                        <figure class="course-thumb">
                            <img src="images/img1.jpg" alt="">
                            <strong class="ribbon">$119.00</strong>

                            <div class="info-area">
                                <ul>
                                    <li>
                                        <a href="#" class="sell" data-toggle="tooltip" data-placement="top" title="Sell">Sell</a>
                                    </li>
                                    <li>
                                        <a href="#" class="used" data-toggle="tooltip" data-placement="top" title="USED">New</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="meta-area">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Gallery"><i class="ti-camera"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="ti-heart"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="ti-dashboard"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </figure>
                        <div class="course-content">
                            <h3><a href="yacht-details.html">Aquavita</a></h3>
                            <hr class="my-2">
                            <dl class="row m-0">
                                <dt class="product-item-part col-xs-5">length</dt>
                                <dd class="col-xs-7">80&nbsp;m</dd>
                                <dt class="product-item-part col-xs-5">Price</dt>
                                <dd class="col-xs-7">
                                    POA </dd>
                                <dt class="product-item-part col-xs-5">shipyard</dt>
                                <dd class="col-xs-7">
                                    Yachtley
                                </dd>
                                <dt class="product-item-part col-xs-5">build/refit</dt>
                                <dd class="col-xs-7">2017</dd>
                            </dl>
                            <div class="enroll row m-0">
                                <div class="ratings">
                                    <span class="total-students"><i class="ti-user"></i> 220 Customers</span>

                                </div>
                                <div class="course-meta text-left">
                                    <a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a>
                                    <span class="enrolled">(130)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-course">
                        <figure class="course-thumb">
                            <img src="#" alt="">
                            <strong class="ribbon">$299.00</strong>

                            <div class="info-area">
                                <ul>
                                    <li>
                                        <a href="#" class="rent" data-toggle="tooltip" data-placement="top" title="Rent">Rent</a>
                                    </li>
                                    <li>
                                        <a href="#" class="used" data-toggle="tooltip" data-placement="top" title="USED">USED</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="meta-area">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Gallery"><i class="ti-camera"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="ti-heart"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="ti-dashboard"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </figure>
                        <div class="course-content">
                            <h3><a href="yacht-details.html">Here Comes Then Sun</a></h3>
                            <hr class="my-2">
                            <dl class="row m-0">
                                <dt class="product-item-part col-xs-5">length</dt>
                                <dd class="col-xs-7">80&nbsp;m</dd>
                                <dt class="product-item-part col-xs-5">Price</dt>
                                <dd class="col-xs-7">
                                    POA </dd>
                                <dt class="product-item-part col-xs-5">shipyard</dt>
                                <dd class="col-xs-7">
                                    Yachtley
                                </dd>
                                <dt class="product-item-part col-xs-5">build/refit</dt>
                                <dd class="col-xs-7">2017</dd>
                            </dl>
                            <div class="enroll row m-0">
                                <div class="ratings">
                                    <span class="total-students"><i class="ti-user"></i> 220 Customers</span>

                                </div>
                                <div class="course-meta text-left">
                                    <a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a>
                                    <span class="enrolled">(130)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-course">
                        <figure class="course-thumb">
                            <img src="images/img3.jpg" alt="">
                            <strong class="ribbon">$99.00</strong>

                            <div class="info-area">
                                <ul>
                                    <li>
                                        <a href="#" class="sell" data-toggle="tooltip" data-placement="top" title="Sell">Sell</a>
                                    </li>
                                    <li>
                                        <a href="#" class="used" data-toggle="tooltip" data-placement="top" title="USED">USED</a>
                                    </li>
                                    <li>
                                        <a href="#" class="sold" data-toggle="tooltip" data-placement="top" title="SOLD">SOLD</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="meta-area">
                                <ul>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Gallery"><i class="ti-camera"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Favourite"><i class="ti-heart"></i> </a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><i class="ti-dashboard"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </figure>
                        <div class="course-content">
                            <h3><a href="yacht-details.html">Amaryllis</a></h3>
                            <hr class="my-2">
                            <dl class="row m-0">
                                <dt class="product-item-part col-xs-5">length</dt>
                                <dd class="col-xs-7">80&nbsp;m</dd>
                                <dt class="product-item-part col-xs-5">Price</dt>
                                <dd class="col-xs-7">
                                    POA </dd>
                                <dt class="product-item-part col-xs-5">shipyard</dt>
                                <dd class="col-xs-7">
                                    Yachtley
                                </dd>
                                <dt class="product-item-part col-xs-5">build/refit</dt>
                                <dd class="col-xs-7">2017</dd>
                            </dl>
                            <div class="enroll row m-0">
                                <div class="ratings">
                                    <span class="total-students"><i class="ti-user"></i> 220 Customers</span>

                                </div>
                                <div class="course-meta text-left">
                                    <a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a>
                                    <span class="enrolled">(130)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Courses section end -->

    <section class="work v2 bg-bluewhite d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">What We Do </span>
                        <h3 class="sec-title">Simple, fast and Hassle Free payments </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="process p-0 v2">
                        <ul>
                            <li class="card mb-4">
                                <span class="icon-bg">
                                    <i class="ti-signal"></i>
                                </span>
                                <h5>Phone Recharge &amp; DTH</h5>
                                <p>Their over fly creature first fish fruitful fourth our very thing said man our land</p>
                            </li>
                            <li class="card mb-4">
                                <span class="icon-bg">
                                    <i class="ti-calendar"></i>
                                </span>
                                <h5><a href="#">Bill Payments </a></h5>
                                <p>Their over fly creature first fish fruitful fourth our very thing said man our land</p>
                            </li>
                            <li class="card mb-4">
                                <span class="icon-bg">
                                    <i class="ti-user"></i>
                                </span>
                                <h5><a href="#">Bus Tickets </a></h5>
                                <p>Their over fly creature first fish fruitful fourth our very thing said man our land</p>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="process p-0 v2">
                        <ul>
                            <li class="card mb-4">
                                <span class="icon-bg">
                                    <i class="ti-palette"></i>
                                </span>
                                <h5><a href="#">Shopping in Local Stores </a></h5>
                                <p>Their over fly creature first fish fruitful fourth our very thing said man our land</p>
                            </li>
                            <li class="card mb-4">
                                <span class="icon-bg">
                                    <i class="ti-home"></i>
                                </span>
                                <h5><a href="#">Boost your finances</a></h5>
                                <p>Their over fly creature first fish fruitful fourth our very thing said man our land</p>
                            </li>
                            <li class="card mb-4">
                                <span class="icon-bg">
                                    <i class="ti-layers"></i>
                                </span>
                                <h5><a href="#">Transfer money to Bank</a></h5>
                                <p>Their over fly creature first fish fruitful fourth our very thing said man our land</p>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- Start Testimonial Section -->
    <section class="testimonial d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline text-white">Testimonials</span>
                        <h3 class="sec-title text-white">What About <br> Our Users Say About Service</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-12 m-auto">
                    <div class="testimonialBox">
                        <span class="quote-sign"><i class="ti-quote-left"></i></span>
                        <div class="test-caro owl-carousel owl-loaded owl-drag" data-slider-id="1">
                            <div class="owl-stage-outer">
                                <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 2580px;">
                                    <div class="owl-item active" style="width: 860px;">
                                        <div class="single-testimonial">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore eco dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                            <div class="testimonial-user">
                                                <img src="#" class="avatar-small circle" alt="">
                                                <strong>Sansa Stark</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-item" style="width: 860px;">
                                        <div class="single-testimonial">
                                            <p>Ypsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore eco dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                            <div class="testimonial-user">
                                                <img src="#" class="avatar-small circle" alt="">
                                                <strong>Linda heiday</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="owl-item" style="width: 860px;">
                                        <div class="single-testimonial">
                                            <p>Qonsectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore eco dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                            <div class="testimonial-user">
                                                <img src="#" class="avatar-small circle" alt="">
                                                <strong>Anna Gunn</strong>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Testimonial Section -->
    
     <!-- Top section start -->
    <section class="tutor bg-offwhite d-none">
        <div class="container">
            <div class="row align-items-center mb-5">
                <div class="col-md">
                    <h3>Find The<br> Top Rated Yacht</h3>
                </div>
                <div class="col-md">
                    <form class="subject-searchform ml-md-auto">
                        <input type="text" placeholder="Search your subject">
                        <button type="submit"><i class="ti-search"></i></button>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="tutor-info">
                        <div class="video-box">
                            <img src="images/img1.jpg" alt="">
                            <a href="#" class="video-btn"><img src="#" alt=""></a>
                        </div>
                        <div class="infoBox">
                            <div class="details">
                                <img src="#" alt="" class="avatar-small circle">
                                <h4>Lisa Sordan</h4>
                                <span class="skills">535 Whitewater Ct, Fenton, MO, 63026</span>
                                <div class="ratings">
                                    <a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a><a href="#"><i class="ti-star"></i>
                                    </a>
                                    <span>70,250 view</span>
                                </div>
                            </div>
                            <div class="hire">
                                <p><strong>$1050</strong> /Per Week</p>
                                <a href="#" class="btn btn-filled">Rent Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="tutor-list">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="#" alt="">
                                    <h5>David Kymen</h5>
                                    <span class="skills">535 Whitewater Ct, Fenton, MO, 63026</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="#" alt="">
                                    <h5>Jessi Green</h5>
                                    <span class="skills">2400 Grove St S, Saint Petersburg, FL, 33705 </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="#" alt="">
                                    <h5>Adam wood</h5>
                                    <span class="skills">1430 Educators Way, Fullerton, CA, 92835</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="#" alt="">
                                    <h5>Cristofer Bin</h5>
                                    <span class="skills">652 S Kentucky Ave, Madisonville, KY, 42431</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="#" alt="">
                                    <h5>Lia Hook</h5>
                                    <span class="skills">3291 Fowlstown Rd, Bainbridge, GA, 39819</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Top section end -->

    <!-- Call to action section start -->
    <section class="callto-action d-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8">
                    <h3>Want To Become a Agent</h3>
                    <p>Success And Spirit In Our Comapny</p>
                </div>
                <div class="col-md-4 text-md-right">
                    <a href="#" class="btn btn-default">Register Now</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Call to action section end -->

    <!-- Nearby students section start -->
    <section class="nearby-students d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">Find nearest Yacht</span>
                        <h3 class="sec-title">We Work Best When We Work Together</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="student-search-form">
                        <h3>Match nearest</h3>
                        <form action="#">
                            <input type="text" placeholder="Enter Yacht Location">
                            <input type="text" placeholder="Enter Yacht Type">
                            <input type="text" placeholder="Enter Zipcode">
                            <button type="submit" class="btn btn-filled">Find Yacht</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Nearby tutors section end -->

    <!-- Certification section start -->
    <section class="certification bg-offwhite">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">Our </span>
                        <h3 class="sec-title">Clients</h3>
                    </div>
                </div>
            </div>
            <div class="partners-caro owl-carousel">
                <a href="#"><img src="images/260x170.jpg" alt=""></a>
                <a href="#"><img src="images/260x170.jpg" alt=""></a>
                <a href="#"><img src="images/260x170.jpg" alt=""></a>
                <a href="#"><img src="images/260x170.jpg" alt=""></a>
            </div>
        </div>
    </section>
    <!-- Certification section end -->

    <!-- Blog section start -->
    <section class="blog d-none">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">What’s new</span>
                        <h3 class="sec-title">Every Single Update From <br>Our Blog Page</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="post-item">
                        <img src="images/post/1.jpg" alt="">
                        <div class="post-content">
                            <div class="meta-tags">
                                <a href="#" class="post-meta category">creative
                                </a> | <a href="#" class="post-meta date">07 July, 2019</a>
                            </div>
                            <h3 class="post-title"><a href="blog-details.html">Own may face grass dot
                                    subdue brought</a></h3>
                            <div class="meta-tags">
                                <a href="#" class="post-meta category"><i class="ti-package"></i>Women Fashion</a><a href="#" class="post-meta commentCount"><i class="ti-comments"></i>2 Comments</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="post-item">
                        <img src="#" alt="">
                        <div class="post-content">
                            <div class="meta-tags">
                                <a href="#" class="post-meta category">technology
                                </a> | <a href="#" class="post-meta date">07 June, 2019</a>
                            </div>
                            <h3 class="post-title"><a href="blog-details.html">Dolores quis commodi ratione fugit optio</a></h3>
                            <div class="meta-tags">
                                <a href="#" class="post-meta category"><i class="ti-package"></i>creative</a><a href="#" class="post-meta commentCount"><i class="ti-comments"></i>2 Comments</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="post-item">
                        <img src="images/post/3.jpg" alt="">
                        <div class="post-content">
                            <div class="meta-tags">
                                <a href="#" class="post-meta category">Marketing
                                </a> | <a href="#" class="post-meta date">13 Feb, 2019</a>
                            </div>
                            <h3 class="post-title"><a href="blog-details.html">Repellat delectus maiores accusantium</a></h3>
                            <div class="meta-tags">
                                <a href="#" class="post-meta category"><i class="ti-package"></i>Women Fashion</a>
                                <a href="#" class="post-meta commentCount"><i class="ti-comments"></i>2 Comments</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog section end -->