<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = "Services | Let's boat Miami";
    $this->params['activeLink'] = "services";
?>

    <!-- Page feature start -->
    <section class="page-feature py-5">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-6">
                    <h2 class=" text-left">Services</h2>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb text-right">
                        <span>Home</span>
                        <span>/ Services</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page feature end -->

    <!-- About section start -->
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <!-- <span class="tagline"></span> -->
                        <h3 class="sec-title">Services</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="<?= Yii::getAlias('@web') ?>/images/800x600.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <div class="entry-content">
                        <h3>Servicio 1.</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, quas! Corporis ipsa esse, soluta deserunt pariatur odit, dicta laboriosam vero sint ullam animi quae laborum numquam officia saepe laudantium quos?.</p>
                        <h6>texto servicio 1.</h6>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam sequi praesentium numquam officia temporibus debitis fugit aspernatur, suscipit minus sint dolore reiciendis sunt, dolorum, vel dolores illo perspiciatis a reprehenderit.?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!-- About section end -->



    <!-- Mision start -->
    <section class="certification bg-offwhite">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="entry-content">
                        <h3>Servicio 2.</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, quas! Corporis ipsa esse, soluta deserunt pariatur odit, dicta laboriosam vero sint ullam animi quae laborum numquam officia saepe laudantium quos?.</p>
                        <h6>texto servicio 2.</h6>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam sequi praesentium numquam officia temporibus debitis fugit aspernatur, suscipit minus sint dolore reiciendis sunt, dolorum, vel dolores illo perspiciatis a reprehenderit.?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?= Yii::getAlias('@web') ?>/images/800x600.jpg" alt="">
                </div>
                
                
            </div>
        </div>
    </section>
    <!-- Mision end -->

    <!-- Vision start -->
    <section class="certification">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="<?= Yii::getAlias('@web') ?>/images/800x600.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <div class="entry-content">
                        <h3>Servicio 3.</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, quas! Corporis ipsa esse, soluta deserunt pariatur odit, dicta laboriosam vero sint ullam animi quae laborum numquam officia saepe laudantium quos?.</p>
                        <h6>texto servicio 3.</h6>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam sequi praesentium numquam officia temporibus debitis fugit aspernatur, suscipit minus sint dolore reiciendis sunt, dolorum, vel dolores illo perspiciatis a reprehenderit.?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </section>
    <!-- Vision end -->



    <!-- valores start -->
    <section class="certification bg-offwhite">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <div class="entry-content">
                        <h3>Servicio N....</h3>
                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat, quas! Corporis ipsa esse, soluta deserunt pariatur odit, dicta laboriosam vero sint ullam animi quae laborum numquam officia saepe laudantium quos?.</p>
                        <h6>texto servicio N....</h6>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam sequi praesentium numquam officia temporibus debitis fugit aspernatur, suscipit minus sint dolore reiciendis sunt, dolorum, vel dolores illo perspiciatis a reprehenderit.?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?= Yii::getAlias('@web') ?>/images/800x600.jpg" alt="">
                </div>
                
                
            </div>
        </div>
    </section>
    <!-- valores end -->