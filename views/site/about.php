<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = "About | Let's boat Miami";
    $this->params['activeLink'] = "about";
?>

<!-- Header strat -->

<!-- Page feature start -->
    <section class="page-feature py-5">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-6">
                    <h2 class=" text-left">About Us</h2>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb text-right">

                        <span>Home</span>
                        <span>/ About</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- Page feature end -->

<!-- About section start -->
    <section class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <!-- <span class="tagline"></span> -->
                        <h3 class="sec-title">About us</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img src="<?= Yii::getAlias('@web') ?>/images/800x600.jpg" alt="">
                </div>
                <div class="col-md-6">
                    <div class="entry-content">
                        <h3>Perspiciatis unde omnis iste natus error inventore.</h3>
                        <p>Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        <h6>Dolor sit amet consectetur adipisicing elit. Unde, quasi.</h6>
                        <p>Nemo dolores, iusto pariatur corporis quis ullam harum voluptate porro officiis aliquam quas explicabo?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<!-- About section end -->



<!-- Mision start -->
    <section class="certification bg-offwhite">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <!-- <span class="tagline"></span> -->
                        <h3 class="sec-title">Mision</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="entry-content">
                        <h3>Texto mision.</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem cupiditate tenetur minus aperiam ab odit recusandae laborum alias praesentium, corporis quam quas veniam minima vel quasi commodi sunt vitae labore..</p>
                        <h6>texto mision 2.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae cupiditate natus officiis provident delectus quos? Vero animi placeat totam? Nisi cupiditate eveniet asperiores adipisci non nobis libero nam consequatur voluptatem?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<!-- Mision end -->

<!-- Vision start -->
    <section class="certification">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <!-- <span class="tagline"></span> -->
                        <h3 class="sec-title">Vision</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="entry-content">
                        <h3>Texto vision.</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem cupiditate tenetur minus aperiam ab odit recusandae laborum alias praesentium, corporis quam quas veniam minima vel quasi commodi sunt vitae labore..</p>
                        <h6>texto v 2.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae cupiditate natus officiis provident delectus quos? Vero animi placeat totam? Nisi cupiditate eveniet asperiores adipisci non nobis libero nam consequatur voluptatem?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<!-- Vision end -->



<!-- valores start -->
    <section class="certification bg-offwhite">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <!-- <span class="tagline"></span> -->
                        <h3 class="sec-title">Valores</h3>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="entry-content">
                        <h3>Texto Valores.</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Autem cupiditate tenetur minus aperiam ab odit recusandae laborum alias praesentium, corporis quam quas veniam minima vel quasi commodi sunt vitae labore..</p>
                        <h6>texto Valores 2.</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae cupiditate natus officiis provident delectus quos? Vero animi placeat totam? Nisi cupiditate eveniet asperiores adipisci non nobis libero nam consequatur voluptatem?</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias similique, consequuntur incidunt at repellendus ratione totam excepturi iste. Similique ipsum numquam recusandae, eligendi maiores temporibus officia harum labore deleniti quasi.</p>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<!-- valores end -->
