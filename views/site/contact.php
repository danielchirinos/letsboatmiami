<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = "Contact | Let's boat Miami";
    $this->params['activeLink'] = "contact";
?>

    <!-- Page feature start -->
    <section class="page-feature py-5">
        <div class="container text-center">
            <div class="row">
                <div class="col-md-6">
                    <h2 class=" text-left">Contact Us</h2>
                </div>
                <div class="col-md-6">
                    <div class="breadcrumb text-right">
                        <span>Home</span>
                        <span>/ Contact Us</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Page feature end -->

    <!-- Contact section start -->
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-md-9 m-auto text-center">
                    <div class="sec-heading">
                        <span class="tagline">Contact us</span>
                        <h3 class="sec-title">If you have any query, <br>leave us a message</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <form action="http://davit.themeies.com/home-01.html">
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <label for="f_name">Nombre</label>
                                <input type="text" class="form-control" id="f_name" placeholder="Nombre" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="l_name">Apellido</label>
                                <input type="text" class="form-control" id="l_name" placeholder="Apellido">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">email</label>
                                <input type="email" class="form-control" id="email" placeholder="Email" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="phone">Numero de telefono</label>
                                <input type="text" class="form-control" id="phone" placeholder="Numero de telefono">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message">Mensaje</label>
                            <textarea name="message" class="form-control" id="message" placeholder="Mensaje" required=""></textarea>
                        </div>
                        <button type="submit" class="btn btn-default">Enviar mensaje</button>
                    </form>
                </div>
                <div class="col-md-4">
                    <aside class="sidebar">
                        <div class="widget contact-info">
                            <h3 class="widget-title">Informacion de contacto</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure quod accusamus est obcaecati eum culpa in explicabo cupiditate animi corporis.</p>
                            <div>
                                <h6>Numeros de telefono</h6>
                                <a href="tel:+12345678912">+000 000 000</a>
                                <a href="tel:+32176542198">+000 000 001</a>
                            </div>
                            <div>
                                <h6>Email Us</h6>
                                <a href="mailto:email1@miami.com">email1@miami.com</a>
                                <a href="mailto:email2@miami.com">email2@miami.com</a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
