<?php


use yii\helpers\Html;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
$Pagina = $this->params['activeLink'];
$campo = isset($this->params['campo']) ? $this->params['campo'] : "" ;
$session = Yii::$app->session;
$idCliente = $session['IdCliente']; 
$nombreCliente = $session['nombreCliente']; 
$version = "0.1";

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="en">


    <title><?= $this->title ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="2Xf0NGIcXBQdnUhP3wMveC5a3EAG151FnB_i3xEHxi4" />
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>

    <!-- Yii::$app->metaComponent->meta()  -->

    <!-- meta para cambiar el color del navegado en los telefonos -->
    <meta name="theme-color" content="#22824B" />

    <!-- Favicon -->
    <link rel="icon" href="<?= Yii::getAlias('@web') ?>/images/favicon-96x96.png" type="image/x-icon" />
    

    <!-- All CSS -->
    <link href="<?= Yii::getAlias('@web') ?>/css/boat.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/themify-icons.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/daterangepicker.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/fonts/flaticon.css" rel="stylesheet">
    <link href="<?= Yii::getAlias('@web') ?>/css/style.css" rel="stylesheet">

<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>

    
    <div class="header-top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-info">
                        <p><i class="ti-mobile"></i> 000-000-0000</p>
                        <p><i class="ti-email"></i> contact@letsinmiami.com</p>
                    </div>
                </div>
                <div class="col-md-6 text-md-right">
            
                    <ul class="login-area d-none">
                        <li><a href="login.html" class="login"><i class="ti-power-off"></i> Login</a></li>
                        <li><a href="signup.html" class="rsgiter"><i class="ti-plus"></i> Register</a></li>
                    </ul>
                </div>
            </div>  
        </div><!-- END .CONTAINER -->
    </div>

    <!-- Header strat -->
    <header class="header  p-0">
        <div class="container">
            <nav class="navbar">
                <!-- Site logo -->
                <a href="home-01.html" class="logo">
                    <span class="flaticon-yacht-1 d-none"><span>name</span></span>
                    <img src="<?= Yii::getAlias("@web") ?>/images/logolbm.jpeg" alt="">
                </a>
                <a href="javascript:void(0);" id="mobile-menu-toggler">
                    <i class="ti-align-justify"></i>
                </a>
                <ul class="navbar-nav">
                    <li >
                        <a href="<?= Yii::getAlias('@web') ?>/site/index"  class="<?= $Pagina == "index" ? "active-link" : "" ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?= Yii::getAlias('@web') ?>/site/about" class="<?= $Pagina == "about" ? "active-link" : "" ?>" >About </a>
                        </li>
                    <li>
                        <a href="<?= Yii::getAlias('@web') ?>/site/services" class="<?= $Pagina == "services" ? "active-link" : "" ?>">Services</a>
                        </li>
                    <li>
                        <a href="<?= Yii::getAlias('@web') ?>/site/contact" class="<?= $Pagina == "contact" ? "active-link" : "" ?>">Contact</a>
                    </li>
                    
                </ul>
            </nav>
        </div>
    </header>
    <!-- Header strat -->

    <?= $content ?>

    <!-- Footer strat -->
    <footer class="footer">
        <div class="foo-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="widget widget-navigation">
                            <h4 class="widget-title">About us</h4>
                            <div class="widget-content">
                                <p>texto 1.</p>
                                <ul class="social-media">
                                    <li><a href="#"><i class="ti-facebook"></i></a></li>
                                    <li><a href="#"><i class="ti-twitter"></i></a></li>
                                    <li><a href="#"><i class="ti-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget widget-navigation">
                            <h4 class="widget-title">Links</h4>
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget widget-navigation">
                            <h4 class="widget-title">texto 3</h4>
                            <ul class="d-none">
                                <li><a href="#">Faq</a></li>
                                <li><a href="#">Privacy policy</a></li>
                                <li><a href="#">Payment options</a></li>
                                <li><a href="#">Fees and Funding</a></li>
                                <li><a href="#">Terms and conditions</a></li>
                            </ul>
                            <ul>
                                <li><a href="#"></a></li>
                                <li><a href="#">Privacy policy</a></li>
                                <li><a href="#">Payment options</a></li>
                                <li><a href="#">Fees and Funding</a></li>
                                <li><a href="#">Terms and conditions</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="widget widget-navigation">
                            <h4 class="widget-title">texto 4</h4>
                            <ul>
                                <li><a href="#">11</a></li>
                                <li><a href="#">22</a></li>
                                <li><a href="#">33</a></li>
                                <li><a href="#">44</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="foo-btm">
            <div class="container">
                <p class="copyright">Copyright © <script>
                        document.write(new Date().getFullYear());
                    </script><a href="#"> Espacio Interactivo</a>. Todos los derechos reservados.</p>
            </div>
        </div>

    </footer>
    <!-- Footer end -->

    <!-- JS -->
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery-3.3.1.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/jquery-ui.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/owl.carousel.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/bootstrap-select.min.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/daterangepicker.js"></script>
    <script src="<?= Yii::getAlias('@web') ?>/js/scripts.js"></script>

    <!-- WhatsHelp.io widget -->
    <script type="text/javascript">
        (function () {
            var options = {
                whatsapp: "+56 9 9564 8173", // WhatsApp number
                call_to_action: "Escribenos ⛵🚤⚓", // Call to action
                button_color: "#FF6550", // Color of button
                position: "right", // Position may be 'right' or 'left'
            };
            var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
            s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
            var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
        })();
    </script>
    <!-- /WhatsHelp.io widget -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
